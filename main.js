import * as M from "./manager.js";

M.addShutdownTask(()=>{
  return new Promise(function(res, rej) {
    //do something with async
    console.log("Shuting down async ...");
    setTimeout(()=>{
      console.log("async done");
      res();
    },5000);

  });
},30000);//max time

M.addShutdownTask(()=>{
  console.log("Shuting down sync");
},30000);//max time

M.shutdown().then(()=>{
  setTimeout(function() { //some save time
    process.exit();
  }, 1000);
}).catch(()=>{
  setTimeout(function() { //shutdown on error with more save time
    process.exit();
  }, 10000);
});
